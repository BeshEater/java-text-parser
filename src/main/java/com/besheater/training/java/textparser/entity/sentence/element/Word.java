package com.besheater.training.java.textparser.entity.sentence.element;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Word extends SentenceElement {
    private static final Logger LOG = LogManager.getLogger();

    public Word(String content) {
        super(content);
        if (!isWord(content)) {
            LOG.error("{} is not a word", content);
            throw new IllegalArgumentException("Not a word");
        }
        LOG.debug("New instance created, content = {}", content);
    }

    public static boolean isWord(String string) {
        return !Symbol.isSymbol(string);
    }
}