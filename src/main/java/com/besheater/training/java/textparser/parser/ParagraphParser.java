package com.besheater.training.java.textparser.parser;

import com.besheater.training.java.textparser.entity.Paragraph;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ParagraphParser {
    private static final Logger LOG = LogManager.getLogger();

    public static List<Paragraph> parse(String text, Locale locale) {
        LOG.debug("Parsing paragraphs with locale: {}, from text: {}", locale, text);

        List<Paragraph> paragraphs = Arrays.stream(text.split("\n"))
                                           .map(p -> makeParagraph(p, locale))
                                           .collect(Collectors.toList());

        LOG.debug("Paragraphs list = {}", paragraphs);
        return paragraphs;
    }

    private static Paragraph makeParagraph(String text, Locale locale) {
        return new Paragraph(SentenceParser.parse(text, locale));
    }
}