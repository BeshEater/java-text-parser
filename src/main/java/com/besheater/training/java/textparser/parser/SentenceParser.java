package com.besheater.training.java.textparser.parser;

import com.besheater.training.java.textparser.entity.sentence.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SentenceParser {
    private static final Logger LOG = LogManager.getLogger();

    public static List<Sentence> parse(String text, Locale locale) {
        LOG.debug("Parsing sentences with locale: {}, from text: {}", locale, text);

        List<Sentence> sentences = new ArrayList<>();
        BreakIterator iter = BreakIterator.getSentenceInstance(locale);
        iter.setText(text);
        int start = iter.first();
        for (int end = iter.next(); end != BreakIterator.DONE; start = end, end = iter.next()) {
            String sentenceText = text.substring(start, end);
            Sentence sentence = makeSentence(sentenceText, locale);
            sentences.add(sentence);
        }

        LOG.debug("Sentences list = {}", sentences);
        return sentences;
    }

    private static Sentence makeSentence(String text, Locale locale) {
        return new Sentence(SentenceElementParser.parse(text, locale));
    }
}