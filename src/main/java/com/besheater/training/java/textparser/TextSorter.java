package com.besheater.training.java.textparser;

import com.besheater.training.java.textparser.entity.Paragraph;
import com.besheater.training.java.textparser.entity.Text;
import com.besheater.training.java.textparser.entity.sentence.Sentence;
import com.besheater.training.java.textparser.entity.sentence.element.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class TextSorter {
    private static final Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {
        InputStream inputStream = TextSorter.class.getResourceAsStream("/sampleText.txt");
        String sampleText = readAll(inputStream, StandardCharsets.UTF_8);
        Text text = new Text(sampleText, Locale.US);

        System.out.println("***** Original text: *****");
        System.out.println(sampleText);

        System.out.println("\n***** Text recreated from Text object: *****");
        System.out.println(text);

        System.out.println("\n***** Text with paragraphs sorted by sentences count: *****");
        System.out.println(sortParagraphsBySentencesCount(text));

        System.out.println("\n***** Text with words sorted by length in each sentence: *****");
        System.out.println(sortWordsInSentencesByLength(text));

        System.out.println("\n***** Text with sentences sorted by word count in each paragraph: *****");
        System.out.println(sortSentencesByWordCountInEachParagraph(text));
    }

    public static Text sortParagraphsBySentencesCount(Text text) {
        return text.getParagraphs().stream()
                                   .sorted(comparingInt(p -> p.getSentences().size()))
                                   .collect(collectingAndThen(toList(), Text::new));
    }

    public static Text sortWordsInSentencesByLength(Text text) {
        return text.getParagraphs().stream()
                                   .map(TextSorter::sortWordsInSentencesByLength)
                                   .collect(collectingAndThen(toList(), Text::new));
    }
    public static Text sortSentencesByWordCountInEachParagraph(Text text) {
        return text.getParagraphs().stream()
                                   .map(TextSorter::sortSentencesByWordCount)
                                   .collect(collectingAndThen(toList(), Text::new));
    }

    public static Paragraph sortWordsInSentencesByLength(Paragraph paragraph) {
        return paragraph.getSentences().stream()
                                       .map(TextSorter::sortWordsByLength)
                                       .collect(collectingAndThen(toList(), Paragraph::new));
    }

    public static Paragraph sortSentencesByWordCount(Paragraph paragraph) {
        return paragraph.getSentences().stream()
                                       .sorted(comparingInt(s -> s.getWords().size()))
                                       .collect(collectingAndThen(toList(), Paragraph::new));
    }

    public static Sentence sortWordsByLength(Sentence sentence) {
        List<Word> sortedWords = sentence.getWords().stream()
                                                    .sorted(comparingInt(Word::length))
                                                    .collect(toList());
        return Sentence.makeOrdinarySentence(sortedWords);
    }

    public static String readAll(InputStream inputStream, Charset charset) {
        try (Scanner scanner = new Scanner(inputStream, charset.name())) {
            return scanner.useDelimiter("\\A").next();
        }
    }
}