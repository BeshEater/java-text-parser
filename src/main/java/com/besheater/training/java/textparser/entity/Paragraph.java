package com.besheater.training.java.textparser.entity;

import com.besheater.training.java.textparser.entity.sentence.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class Paragraph implements CharSequenceComponent {
    private static final Logger LOG = LogManager.getLogger();

    private final List<Sentence> sentences;

    public Paragraph(List<Sentence> sentences) {
        this.sentences = sentences;
        LOG.debug("New instance created, sentences = {}", sentences);
    }

    public List<Sentence> getSentences() {
        return Collections.unmodifiableList(sentences);
    }

    @Override
    public String toString() {
        return String.join("", sentences);
    }
}