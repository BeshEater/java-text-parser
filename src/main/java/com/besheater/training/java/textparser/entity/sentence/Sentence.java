package com.besheater.training.java.textparser.entity.sentence;

import com.besheater.training.java.textparser.entity.CharSequenceComponent;
import com.besheater.training.java.textparser.entity.sentence.element.SentenceElement;
import com.besheater.training.java.textparser.entity.sentence.element.Symbol;
import com.besheater.training.java.textparser.entity.sentence.element.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Sentence implements CharSequenceComponent {
    private static final Logger LOG = LogManager.getLogger();

    private final List<SentenceElement> elements;

    public Sentence(List<SentenceElement> elements) {
        this.elements = elements;
        LOG.debug("New instance created, elements = {}", elements);
    }

    public List<SentenceElement> getElements() {
        return Collections.unmodifiableList(elements);
    }

    public List<Word> getWords() {
        return elements.stream().filter(Word.class::isInstance)
                                .map(Word.class::cast)
                                .collect(Collectors.toList());
    }

    public List<Symbol> getSymbols() {
        return elements.stream().filter(Symbol.class::isInstance)
                                .map(Symbol.class::cast)
                                .collect(Collectors.toList());
    }

    public static Sentence makeOrdinarySentence(List<Word> words) {
        Symbol whiteSpace = new Symbol(" ");
        Symbol dot = new Symbol(".");
        List<SentenceElement> elements = new ArrayList<>();
        for (Word word : words) {
            elements.add(word);
            elements.add(whiteSpace);
        }
        elements.remove(elements.lastIndexOf(whiteSpace));
        elements.add(dot);
        elements.add(whiteSpace);
        return new Sentence(elements);
    }

    @Override
    public String toString() {
        return String.join("", elements);
    }
}