package com.besheater.training.java.textparser.entity.sentence.element;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Symbol extends SentenceElement{
    private static final Logger LOG = LogManager.getLogger();

    public Symbol(String content) {
        super(content);
        if (!isSymbol(content)) {
            LOG.error("{} is not a symbol", content);
            throw new IllegalArgumentException("Not a symbol");
        }
        LOG.debug("New instance created, content = {}", content);
    }

    public static boolean isSymbol(String string) {
        return string.matches("\\W+");
    }

    public boolean isWhiteSpace() {
        return toString().matches("\\s+");
    }

    public boolean isPunctuation() {
        return toString().matches("\\p{Punct}+");
    }
}