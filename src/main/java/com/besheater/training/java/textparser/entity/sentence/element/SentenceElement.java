package com.besheater.training.java.textparser.entity.sentence.element;

import com.besheater.training.java.textparser.entity.CharSequenceComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class SentenceElement implements CharSequenceComponent {
    private static final Logger LOG = LogManager.getLogger();

    private final String content;

    public SentenceElement(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content;
    }
}