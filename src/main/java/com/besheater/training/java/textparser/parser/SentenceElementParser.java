package com.besheater.training.java.textparser.parser;

import com.besheater.training.java.textparser.entity.sentence.element.SentenceElement;
import com.besheater.training.java.textparser.entity.sentence.element.Symbol;
import com.besheater.training.java.textparser.entity.sentence.element.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SentenceElementParser {

    private static final Logger LOG = LogManager.getLogger();

    public static List<SentenceElement> parse(String text, Locale locale) {
        LOG.debug("Parsing elements with locale: {}, from text: {}", locale, text);

        List<SentenceElement> elements = new ArrayList<>();
        BreakIterator iter = BreakIterator.getWordInstance(locale);
        iter.setText(text);
        int start = iter.first();
        for (int end = iter.next(); end != BreakIterator.DONE; start = end, end = iter.next()) {
            String elementText = text.substring(start, end);
            SentenceElement element = makeElement(elementText);
            elements.add(element);
        }

        LOG.debug("Elements list = {}", elements);
        return elements;
    }

    private static SentenceElement makeElement(String content) {
        if (Word.isWord(content)) {
            return new Word(content);
        }
        if (Symbol.isSymbol(content)) {
            return new Symbol(content);
        }
        return null;
    }
}