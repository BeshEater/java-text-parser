package com.besheater.training.java.textparser.entity;

import com.besheater.training.java.textparser.parser.ParagraphParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Text implements CharSequenceComponent {
    private static final Logger LOG = LogManager.getLogger();

    private final List<Paragraph> paragraphs;

    public Text(String text, Locale locale) {
        this(ParagraphParser.parse(text, locale));
    }

    public Text(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
        LOG.debug("New instance created, paragraphs = {}", paragraphs);
    }

    public List<Paragraph> getParagraphs() {
        return Collections.unmodifiableList(paragraphs);
    }

    @Override
    public String toString() {
        return String.join("\n", paragraphs);
    }
}